#!/bin/bash
# Global library
export TAGADA_HOME=$PWD

# Specs
export SPECS=$TAGADA_HOME/tagada-specs
  export BUNDLE_GEMFILE=$SPECS/Gemfile

# Tools
export TOOLS=$TAGADA_HOME/tagada-tools/target/release/tagada-tools

# Step2
export STEP2=$TAGADA_HOME/tagada-step2/build/libs/tagada-step2-1.0.0-all.jar

# External dependencies
export DEPS=$TAGADA_HOME/tagada-deps

  export MINIZINC=$DEPS/MiniZinc/latest/bin/minizinc

  export PICAT=$DEPS/Picat/latest/picat
  export FZN_PICAT_SAT=$DEPS/Picat/latest/lib/fzn_picat_sat.pi

  export ESPRESSO_AB=espresso

# export GUROBI_DLL=
# export FZN_ORTOOLS=
