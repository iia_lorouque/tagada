FROM ubuntu:20.04

# Defining default non-root user UID, GID, and name
ARG USER_UID="1000"
ARG USER_GID="1000"
ARG USER_NAME="default"

# Creating default non-user
RUN groupadd -g $USER_GID $USER_NAME\
    && useradd -m -g $USER_GID -u $USER_UID $USER_NAME

# Installing basic packages
RUN apt-get update\
    && apt-get install -y zip unzip curl locales locales-all language-pack-en git build-essential ruby ruby-dev rubygems python3\
        && gem install bundler -v 4.9.3

# Set UTF-8 as default
ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

# Switching to non-root user to install SDKMAN!
USER $USER_UID:$USER_GID

# Downloading SDKMAN!
RUN curl -s "https://get.sdkman.io" | bash
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk update"

# Installing Gradle
ARG GRADLE_VERSION="7.5.1"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install gradle $GRADLE_VERSION"
ENV GRADLE_HOME="/home/default/.sdkman/candidates/gradle/current"
ENV PATH="$GRADLE_HOME/bin:$PATH"

# Installing Java
ARG JAVA_VERSION="17.0.5-tem"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install java $JAVA_VERSION"
ENV JAVA_HOME="/home/default/.sdkman/candidates/java/current"
ENV PATH="$JAVA_HOME/bin:$PATH"

# Installing Kotlin
ARG KOTLIN_VERSION="1.8.20"
ENV KOTLIN_HOME="/home/default/.sdkman/candidates/kotlin/current"
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh\
    && sdk install kotlin $KOTLIN_VERSION"
ENV PATH="$KOTLIN_HOME/bin:$PATH"

# Installing Tagada
RUN cd /home/default \
   && git clone --recurse-submodules https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lorouque/tagada.git \
   && cd tagada \
   && cd tagada-specs \
   &&    bundle install \
   && cd .. \
   && cd tagada-tools \
   &&   cargo build --release \
   && cd .. \
   && cd tagada-step2 \
   &&   chmod +x gradlew \
   &&   ./gradlew shadowJar \
   && cd ..
