#!/bin/bash
git clone --recurse-submodules https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lorouque/tagada.git \
  && cd tagada

cd tagada-specs
  bundle install
cd ..
cd tagada-tools
  cargo build --release
cd ..
cd tagada-step2
  chmod +x gradlew
  ./gradlew shadowJar
cd ..
cd tagada-deps
  chmod +x ./install_minizinc.sh
  ./install_minizinc.sh
  chmod +x ./install_picat.sh
  ./install_picat.sh
cd ..
source env.sh
cp tagada-tools/operator_info_store-1.0.0.mpk operator_info_store-1.0.0.mpk
