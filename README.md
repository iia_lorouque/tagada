# Tagada

## Clone

```shell
git clone --recurse-submodules https://oauth2:glpat-FrRkPBpFhreZ6zWzxndJ@gitlab.limos.fr/iia_lorouque/tagada.git
```

**TAGADA IS CURRENTLY UNDER A HARD REFACTORING PROCESS. USERS THAT WANT TO GET THE CODE OF [Automatic Generation of Declarative Models for Differential Cryptanalysis](https://hal.science/hal-03320980/file/main.pdf) SHOULD REFER TO [Tagada-v1](https://gitlab.limos.fr/iia_lulibral/tagada/-/tree/1.0?ref_type=tags).**

TAGADA: a Tool for Automatic Generation of Abstraction-based Differential Attacks

![tagada](tagada.png)

## Introduction

The new version of Tagada is splitted in 4 components.

- [`Tagada-Specs`](https://gitlab.limos.fr/iia_lorouque/tagada-specs) contains utilities to describe ciphers in term of Tagada graphs;
- [`Tagada-Tools`](https://gitlab.limos.fr/iia_lulibral/tagada-lib) contains the main core utilities to manipulate the Tipher graphs;
- [`Tagada-Step2`](https://gitlab.limos.fr/iia_lorouque/tadaga-step2) contains the CP tools that handle the instantiation of truncated differentials;
- [`Tagada-Structs`](https://gitlab.limos.fr/iia_lorouque/tagada-structs) is a library that allows to read and write Tagada graphs from several programming languages. For now the supported languages are `Ruby`, `Rust` and `Kotlin/Java`.

## Requirements

### Tagada-Specs

- Ruby
- gem bundler

## Tagada-Tools

- Cargo
- Minizinc
- Espresso-AB (if the user want to use the `cnf` encoding)
- Picat or Gurobi for Truncated Differential Characteristic search
- Graphviz for graph visualization

## Tagada-Step2

- Java >= 17
- Gradle (optional as the user may use the gradle wrapper)

## Installation

## Using `curl`

```sh
curl -L https://gitlab.limos.fr/iia_lorouque/tagada/-/raw/master/install.sh | sh
```

The script will automatically download and build Tagada, however Tagada has some static dependencies that can be installed automatically such as espresso_ab, gurobi or graphviz.

Once the different dependencies the user has to fill the correct path into the `env.sh` file to automatically configure Tagada. An other option is to pass the path has command arguments when it is available.

Once the installation is done the user try to compute some differential characteristics, e.g. for 3 rounds of aes128:

```sh
# Enable automatic bindings
source env.sh
# Generate 3 rounds of AES-128
bundle exec $SPECS/ciphers/rijndael.rb -k 128 -p 128 -r 3 > aes128-3rounds.spec.json
# Convert the specification graph into a differential one
$TOOLS transform derive aes128-3rounds.spec.json > aes128-3rounds.diff.json
# Create a truncated differential graph from the differential graph
$TOOLS transform truncate-differential aes128-3rounds.diff.json -g 5 -d -t > aes128-3rounds.trunc.json
# Search for the best differential characteristic distinguisher in two steps
$TOOLS search best-differential-characteristic aes128-3rounds.diff.json aes128-3rounds.trunc.json picat
```

## Using Docker

The docker image is currently under development.

```sh
 docker build --rm -f Dockerfile -t tagada .
```